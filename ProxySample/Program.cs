﻿using ReRoutor;
using ReRoutor.Clients.Proxies;
using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.Text;

namespace ProxySample
{
    class Program
    {
        static void Main(string[] args)
        {
            Console.WriteLine("This sample project will use multiple HTTPS Proxies");
            Console.WriteLine("What this will do in short is");
            Console.WriteLine("1) Run 1 server at port 8080");
            Console.WriteLine("2) Everytime Proxifier or a socks proxy program wants to connect it will connect to a random proxy");
            Console.WriteLine("3) This will somewhat balance the load on all proxies instead of using 1 proxy for everything");
            Console.WriteLine("4) IpChicken.com will constantly give you a random ip address (IP from 1 of the proxies)");

            ReRouteServer routingServer = new ReRouteServer();

            routingServer.onIncomingConnection += routingServer_onIncomingConnection;
            routingServer.StartServer(8080, typeof(ImageReplaceProxy));

            Process.GetCurrentProcess().WaitForExit();
        }

        static void routingServer_onIncomingConnection(ReRoutor.Clients.RouteSocket Client)
        {
            Client.onConnectionEstablished += Client_onConnectionEstablished;

            //all these proxies are public from www.incloak.com, use at own responsibility

            ImageReplaceProxy proxyClient = Client as ImageReplaceProxy;

            //All Images here
            proxyClient.AddImageLocation("img.webmd.com", "/dtmcms/live/webmd/consumer_assets/site_images/articles/health_tools/cat_skin_problems_slideshow/photolibrary_rf_photo_of_cat_scratching.jpg", false);

            //HTTPS Proxies
            /**/proxyClient.AddProxyServer("212.183.187.61", 3128);
            proxyClient.AddProxyServer("46.149.84.12", 3128);
            proxyClient.AddProxyServer("195.57.230.226", 3128);
            proxyClient.AddProxyServer("80.28.222.210", 3128);
            proxyClient.AddProxyServer("91.209.54.54", 3128);
            proxyClient.AddProxyServer("79.142.93.236", 3128);
            proxyClient.AddProxyServer("91.197.129.18", 3128);
            proxyClient.AddProxyServer("178.158.247.213", 3128);
            proxyClient.AddProxyServer("79.142.93.237", 3128);
            proxyClient.AddProxyServer("79.142.93.238", 3128);
            proxyClient.AddProxyServer("109.106.132.74", 3128);
            proxyClient.AddProxyServer("177.7.4.61", 3128);
            proxyClient.AddProxyServer("201.76.189.10", 3128);
            proxyClient.AddProxyServer("201.55.89.210", 3128);
            proxyClient.AddProxyServer("61.19.42.68", 3128);

            //SOCKS 5
            //proxyClient.AddProxyServer("64.203.163.100", 58287);







            /*proxyClient.AddProxyServer("104.171.246.119", 25461);
            proxyClient.AddProxyServer("101.99.35.61", 18014);
            proxyClient.AddProxyServer("123.30.137.212", 1081);
            proxyClient.AddProxyServer("186.202.153.163", 1081);
            proxyClient.AddProxyServer("118.69.174.36", 1080);
            proxyClient.AddProxyServer("176.36.214.95", 3060);
            proxyClient.AddProxyServer("103.38.82.180", 54652);
            proxyClient.AddProxyServer("121.55.221.193", 22879);
            proxyClient.AddProxyServer("70.184.92.175", 18693);
            proxyClient.AddProxyServer("71.199.56.170", 50435);
            proxyClient.AddProxyServer("115.68.23.192", 60088);
            proxyClient.AddProxyServer("112.227.219.52", 1080);
            proxyClient.AddProxyServer("42.112.20.51", 1080);
            proxyClient.AddProxyServer("42.112.20.140", 1080);
            proxyClient.AddProxyServer("42.112.20.236", 1080);
            proxyClient.AddProxyServer("223.255.178.58", 1080);
            proxyClient.AddProxyServer("121.40.106.36", 8080);
            proxyClient.AddProxyServer("121.40.106.36", 8080);
            proxyClient.AddProxyServer("112.227.219.52", 1080);
            proxyClient.AddProxyServer("37.46.124.70", 1081);
            proxyClient.AddProxyServer("213.85.221.220", 1095);
            proxyClient.AddProxyServer("212.164.221.11", 1095);
            proxyClient.AddProxyServer("115.84.182.9", 1080);
            proxyClient.AddProxyServer("92.127.221.33", 1095);
            proxyClient.AddProxyServer("37.46.124.70", 1081);
            proxyClient.AddProxyServer("94.233.73.71", 1095);
            proxyClient.AddProxyServer("91.246.235.157", 16122);
            proxyClient.AddProxyServer("5.143.48.22", 1095);
            proxyClient.AddProxyServer("92.37.145.87", 1095);
            proxyClient.AddProxyServer("37.46.124.69", 1081);
            proxyClient.AddProxyServer("206.214.122.2", 15569);
            proxyClient.AddProxyServer("77.87.144.84", 18362);
            proxyClient.AddProxyServer("5.143.113.161", 1095);
            proxyClient.AddProxyServer("216.239.188.102", 39200);
            proxyClient.AddProxyServer("183.54.15.58", 1080);
            proxyClient.AddProxyServer("94.23.65.156", 60088);
            proxyClient.AddProxyServer("87.226.175.101", 2235);
            proxyClient.AddProxyServer("206.190.140.53", 44793);
            proxyClient.AddProxyServer("184.155.230.151", 24247);
            proxyClient.AddProxyServer("46.19.45.22", 40742);
            proxyClient.AddProxyServer("206.190.140.53", 43488);
            proxyClient.AddProxyServer("206.190.140.53", 40762);
            proxyClient.AddProxyServer("81.163.69.12", 1095);
            proxyClient.AddProxyServer("71.225.92.117", 33019);
            proxyClient.AddProxyServer("186.202.153.163", 1081);
            proxyClient.AddProxyServer("112.227.219.52", 1080);
            proxyClient.AddProxyServer("42.112.20.236", 1080);
            proxyClient.AddProxyServer("42.112.20.51", 1080);
            proxyClient.AddProxyServer("212.164.221.11", 1095);
            proxyClient.AddProxyServer("121.40.106.36", 8080);
            proxyClient.AddProxyServer("91.246.235.157", 16122);
            proxyClient.AddProxyServer("77.87.144.84", 18362);
            proxyClient.AddProxyServer("115.68.23.192", 60088);
            proxyClient.AddProxyServer("42.112.20.140", 1080);
            proxyClient.AddProxyServer("183.54.15.58", 1080);
            proxyClient.AddProxyServer("78.153.149.113", 1082);
            proxyClient.AddProxyServer("206.190.140.53", 41037);
            proxyClient.AddProxyServer("206.190.140.53", 41972);
            proxyClient.AddProxyServer("206.190.140.53", 48128);
            proxyClient.AddProxyServer("206.190.140.52", 49118);
            proxyClient.AddProxyServer("217.150.201.3", 1082);
            proxyClient.AddProxyServer("121.40.106.36", 8080);
            proxyClient.AddProxyServer("112.227.219.52", 1080);
            proxyClient.AddProxyServer("178.47.139.89", 1095);
            proxyClient.AddProxyServer("42.112.20.51", 1080);
            proxyClient.AddProxyServer("186.202.153.163", 1081);
            proxyClient.AddProxyServer("168.70.54.15", 9999);
            proxyClient.AddProxyServer("81.163.69.12", 1095);
            proxyClient.AddProxyServer("78.153.149.113", 1082);
            proxyClient.AddProxyServer("206.190.140.52", 49118);*/
        }

        static void Client_onConnectionEstablished(ReRoutor.Clients.RouteSocket Client)
        {
            Console.WriteLine("Connected to proxy " + Client.ServerIP.Address + ":" + Client.ServerIP.Port);
        }
    }
}