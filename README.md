# ReRoutor
A Re-Routing library used to re-route the traffic of TCP traffic (soon UDP Traffic)
It can be used for logging, re-routing traffic to a different server, manipulating traffic and more

In the sample project it will contain a simple preview for a game called MineCraft
In this example we will constantly act as a different server "MineCraft Advertisement Client"
We have a list of IP Addresses where the ReRoutor will keep connecting to a different MineCraft server
Making the favorite list in the game look funny by hitting Refresh to act as a different server
