﻿using System;
using System.Collections.Generic;
using System.Net.Sockets;
using System.Text;

namespace ReRoutor.Clients
{
    /// <summary>
    /// The generic client will simply redirect the traffic as a Man In The Middle without manipulations
    /// </summary>
    public class GenericClient : RouteSocket
    {
        public GenericClient(Socket Socket)
            : base(Socket)
        {

        }

        public override void ReceiveDataFromClient(byte[] Data)
        {
            //let the traffic flow normal
            //we received data from the client so we send it to the server
            base.SendDataToServer(Data);
        }

        public override void ReceiveDataFromServer(byte[] Data)
        {
            //let the traffic flow normal
            //we received data from the server so we send it to the client
            base.SendDataToClient(Data);
        }

        public override System.Net.IPEndPoint SetTargetServer()
        {
            return null;
        }

        public override void ConnectionEstablished()
        {

        }

        public override void ConnectionDisconnected()
        {

        }
    }
}