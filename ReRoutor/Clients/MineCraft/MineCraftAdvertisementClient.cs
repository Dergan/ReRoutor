﻿using System;
using System.Collections.Generic;
using System.Net;
using System.Net.Sockets;
using System.Text;

namespace ReRoutor.Clients.MineCraft
{
    /// <summary>
    /// This MineCraft Advertisement Client will keep redirecting the game to different servers
    /// Add this ReRoutor server to your favorites in the game and see the effect
    /// </summary>
    public class MineCraftAdvertisementClient : RouteSocket
    {
        //I'm sorry for the IP Addresses here, they're only used for demostration purposes only
        //If you're the server admin of one of these servers and you want it removed, ask me

        private IPEndPoint[] DemoServers = new IPEndPoint[]
        {
            new IPEndPoint(IPAddress.Parse("31.220.14.172"), 25565), //lava-craft.net
            new IPEndPoint(IPAddress.Parse("192.99.149.116"), 25565), //play.vulcanmc.net
            new IPEndPoint(IPAddress.Parse("104.128.235.142"), 25565), //Vulcanmc.org
            new IPEndPoint(IPAddress.Parse("167.114.119.174"), 25565), //play.koonkraft.net
            new IPEndPoint(IPAddress.Parse("108.178.7.206"), 25565), //us.mineplex.com
            new IPEndPoint(IPAddress.Parse("31.220.14.134"), 25565), //mineverse.com
            new IPEndPoint(IPAddress.Parse("23.238.135.162"), 25565), //Play.gotpvp.com
            new IPEndPoint(IPAddress.Parse("31.220.14.201"), 25565), //Minetime.com
            new IPEndPoint(IPAddress.Parse("31.220.14.131"), 25565), //mc-central.net
            new IPEndPoint(IPAddress.Parse("192.99.39.211"), 25565), //FadeCloud.com
        };

        private Random rnd = new Random(DateTime.Now.Millisecond);

        public MineCraftAdvertisementClient(Socket socket)
            : base(socket)
        {

        }

        public override System.Net.IPEndPoint SetTargetServer()
        {
            return DemoServers[rnd.Next(0, DemoServers.Length)];
        }

        public override void ReceiveDataFromClient(byte[] Data)
        {
            //let the traffic flow normal
            //we received data from the client so we send it to the server
            base.SendDataToServer(Data);
        }

        public override void ReceiveDataFromServer(byte[] Data)
        {
            //let the traffic flow normal
            //we received data from the server so we send it to the client
            base.SendDataToClient(Data);
        }

        public override void ConnectionEstablished()
        {

        }

        public override void ConnectionDisconnected()
        {

        }
    }
}