﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Net.Sockets;
using System.Text;

namespace ReRoutor.Clients.MineCraft
{
    /// <summary>
    /// The MineCraft client used for the game MineCraft
    /// </summary>
    public class MineCraftClient : RouteSocket
    {
        //The Game/Server streams are used for Fragmented Traffic
        private MemoryStream GameStream;
        private MemoryStream ServerStream;

        public MineCraftClient(Socket Socket)
            : base(Socket)
        {
            this.GameStream = new MemoryStream();
            this.ServerStream = new MemoryStream();
        }

        public override void ReceiveDataFromClient(byte[] Data)
        {
            //let the traffic flow normal
            //we received data from the client so we send it to the server
            base.SendDataToServer(Data);
        }

        public override void ReceiveDataFromServer(byte[] Data)
        {
            //let the traffic flow normal
            //we received data from the server so we send it to the client
            base.SendDataToClient(Data);
        }

        public override System.Net.IPEndPoint SetTargetServer()
        {
            return null;
        }

        public override void ConnectionEstablished()
        {

        }

        public override void ConnectionDisconnected()
        {

        }
    }
}