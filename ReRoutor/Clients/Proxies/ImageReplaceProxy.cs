﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Net;
using System.Net.Sockets;
using System.Text;

namespace ReRoutor.Clients.Proxies
{
    /// <summary>
    /// The Image Replace Proxy will change all the pictures, photos to something you specified
    /// </summary>
    public class ImageReplaceProxy : RouteSocket
    {
        private List<ProxyHost> Proxies;
        private Random rnd = new Random();
        private List<ImgLocation> ImgLocations;

        private List<string> ServerTraffic = new List<string>();
        private List<string> ClientTraffic = new List<string>();

        private bool BlockServerTraffic = false;

        public ImageReplaceProxy(Socket socket)

            : base(socket)
        {
            this.Proxies = new List<ProxyHost>();
            this.ImgLocations = new List<ImgLocation>();
        }

        public override void ReceiveDataFromClient(byte[] Data)
        {
            //what we will do here is simple:
            //1. Check if there is a GET request for a image like jpg, png or gif
            //2. Get 1 of the random spoof images
            //3. Send the random spoofed image to the client (Browser)
            //4. Disconnect from client and server (Webserver)
            //5. Spoof successful!

            string HTML = ASCIIEncoding.ASCII.GetString(Data);

            using (StreamReader sr = new StreamReader(new MemoryStream(Data)))
            {
                List<string> AllLines = new List<string>();
                string line = "";
                while ((line = sr.ReadLine()) != null)
                {
                    if (line.StartsWith("GET"))
                    {
                        string[] GETRequest = line.Split(' ');

                        if(GETRequest.Length > 1)
                        {
                            bool ImageRequest = GETRequest[1].ToLower().Contains(".jpg") || GETRequest[1].ToLower().Contains(".png") || GETRequest[1].ToLower().Contains(".gif");

                            if(ImageRequest)
                            {
                                lock (ImgLocations)
                                {
                                    ImgLocation imgLoc = ImgLocations[rnd.Next(0, ImgLocations.Count)];

                                    //send spoofed response
                                    base.SendDataToClient(imgLoc.SpoofResponseBytes);

                                    Disconnect();
                                }

                                BlockServerTraffic = true;
                                return;
                            }
                        }
                    }

                    ClientTraffic.Add("[" + base.ClientPacketsReceived + "] " + line);
                }
            }


            base.SendDataToServer(Data);
        }

        public override void ReceiveDataFromServer(byte[] Data)
        {
            //Let the traffic flow normal
            base.SendDataToClient(Data);
        }

        public override void ConnectionEstablished()
        {

        }

        public override System.Net.IPEndPoint SetTargetServer()
        {
            if (Proxies.Count == 0)
            {
                Disconnect();
                return null;
            }

            ProxyHost RandomProxy = Proxies[rnd.Next(0, Proxies.Count)];
            return new System.Net.IPEndPoint(IPAddress.Parse(RandomProxy.IpAddress), RandomProxy.Port);
        }

        public void AddProxyServer(string IpAddress, int Port)
        {
            lock (Proxies)
            {
                Proxies.Add(new ProxyHost(IpAddress, Port));
            }
        }

        public override void ConnectionDisconnected()
        {

        }

        public void AddImageLocation(string Host, string ImgLocation, bool isHTTPS)
        {
            lock(ImgLocations)
            {
                ImgLocations.Add(new ImgLocation(Host, ImgLocation, isHTTPS));
            }
        }
    }
}