﻿using System;
using System.Collections.Generic;
using System.Net.Sockets;
using System.Text;

namespace ReRoutor.Clients.Proxies
{
    public class ImgLocation
    {
        public string Host { get; private set; }
        public string ImgLoc { get; private set; }
        public int Port { get; private set; }

        public string SpoofResponse { get; private set; }

        public byte[] SpoofResponseBytes
        {
            private set;
            get;
        }

        public ImgLocation(string Host, string ImgLocation, bool isHTTPS)
        {
            this.Host = Host;
            this.ImgLoc = ImgLocation;
            this.Port = isHTTPS ? 443 : 80;
            SetSpoofResponse();
        }

        private void SetSpoofResponse()
        {
            //build the GET Request for the Web Server
            //We need the response in Raw HTML so we're able to directly send this back to the client (WebBrowser)

            StringBuilder HtmlRequest = new StringBuilder();
            HtmlRequest.AppendLine("GET " + ImgLoc + " HTTP/1.1");
            HtmlRequest.AppendLine("Host: " + Host);
            HtmlRequest.AppendLine("User-Agent: Mozilla/5.0 (Windows NT 6.3; WOW64; rv:37.0) Gecko/20100101 Firefox/37.0");
            HtmlRequest.AppendLine("Accept: text/html,application/xhtml+xml,application/xml;q=0.9,*/*;q=0.8");
            HtmlRequest.AppendLine("Accept-Encoding: gzip, deflate");
            HtmlRequest.AppendLine("Connection: Keep-alive");
            HtmlRequest.AppendLine("Cache-Control: max-age=0");
            HtmlRequest.AppendLine("");

            Socket sock = new Socket(AddressFamily.InterNetwork, SocketType.Stream, ProtocolType.Tcp);
            sock.Connect(Host, Port);

            byte[] RequestBytes = ASCIIEncoding.ASCII.GetBytes(HtmlRequest.ToString());
            sock.Send(RequestBytes);

            byte[] ResponseBytes = new byte[1024 * 1024]; //1MB should be enough
            int RecvOffset = 0;
            int Received = 0;

            try
            {
                //Needs futher testing for Receiving but works fine currently
                Received = sock.Receive(ResponseBytes, RecvOffset, ResponseBytes.Length - RecvOffset, SocketFlags.None);
                RecvOffset = Received;
                //while ((Received = sock.Receive(ResponseBytes, RecvOffset, ResponseBytes.Length - RecvOffset, SocketFlags.None)) > 0)
                    //RecvOffset += Received > 0 ? Received : 0;
            }
            catch { }

            Array.Resize(ref ResponseBytes, RecvOffset);

            SpoofResponseBytes = ResponseBytes;
            SpoofResponse = ASCIIEncoding.ASCII.GetString(ResponseBytes, 0, RecvOffset);

        }
    }
}
