﻿using System;
using System.Collections.Generic;
using System.Text;

namespace ReRoutor.Clients.Proxies
{
    internal class ProxyHost
    {
        public string IpAddress { get; private set; }
        public int Port { get; private set; }

        public ProxyHost(string IpAddress, int Port)
        {
            this.IpAddress = IpAddress;
            this.Port = Port;
        }
    }
}
