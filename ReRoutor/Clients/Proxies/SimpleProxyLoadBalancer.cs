﻿using System;
using System.Collections.Generic;
using System.Net;
using System.Net.Sockets;
using System.Text;

namespace ReRoutor.Clients.Proxies
{
    public class SimpleProxyLoadBalancer : RouteSocket
    {
        private List<ProxyHost> Proxies;
        private Random rnd = new Random();

        public SimpleProxyLoadBalancer(Socket socket)
            : base(socket)
        {
            this.Proxies = new List<ProxyHost>();
        }

        public override void ReceiveDataFromClient(byte[] Data)
        {
            //let the traffic flow normal
            //we received data from the client so we send it to the server
            base.SendDataToServer(Data);
        }

        public override void ReceiveDataFromServer(byte[] Data)
        {
            //let the traffic flow normal
            //we received data from the server so we send it to the client
            base.SendDataToClient(Data);
        }

        public override void ConnectionEstablished()
        {

        }

        public override System.Net.IPEndPoint SetTargetServer()
        {
            if (Proxies.Count == 0)
            {
                Disconnect();
                return null;
            }

            ProxyHost RandomProxy = Proxies[rnd.Next(0, Proxies.Count)];
            return new System.Net.IPEndPoint(IPAddress.Parse(RandomProxy.IpAddress), RandomProxy.Port);
        }

        public void AddProxyServer(string IpAddress, int Port)
        {
            lock (Proxies)
            {
                Proxies.Add(new ProxyHost(IpAddress, Port));
            }
        }

        public override void ConnectionDisconnected()
        {

        }
    }
}