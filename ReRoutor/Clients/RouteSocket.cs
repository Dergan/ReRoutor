﻿using System;
using System.Collections.Generic;
using System.Net;
using System.Net.Sockets;
using System.Text;

namespace ReRoutor.Clients
{
    public abstract class RouteSocket
    {
        public const int BUFFER_SIZE = 8192;
        private byte[] ClientBuffer;
        private byte[] ServerBuffer;


        private Socket _socket;
        private Socket _serverSocket;

        /// <summary>
        /// The Socket of e.g. a Game/Browser...
        /// </summary>
        public Socket Socket
        {
            get
            {
                return _socket;
            }
            set
            {
                if (value != null)
                {
                    _socket = value;
                }
            }
        }

        /// <summary>
        /// The Socket which is connected to the real server e.g. a Game Server, WebServer...
        /// </summary>
        public Socket ServerSocket
        {
            get
            {
                return _serverSocket;
            }
            set
            {
                if (value != null)
                {
                    _serverSocket = value;
                }
            }
        }
        public IPEndPoint ClientIP
        {
            get;
            private set;
        }
        public IPEndPoint ServerIP
        {
            get;
            private set;
        }

        public int ClientPacketsReceived { get; internal set; }
        public int ServerPacketsReceived { get; internal set; }

        public event ReceiveDataCallback onReceiveDataFromClient;
        public event ReceiveDataCallback onReceiveDataFromServer;
        public event ConnectionEstablishedCallback onConnectionEstablished;
        public event ConnectionDisconnectedCallback onConnectionDisconnected;
        public event SetTargetServerCallback onSetTargetServer;

        public abstract void ReceiveDataFromClient(byte[] Data);
        public abstract void ReceiveDataFromServer(byte[] Data);
        public abstract void ConnectionEstablished();
        public abstract void ConnectionDisconnected();

        public abstract IPEndPoint SetTargetServer();

        public RouteSocket(Socket Socket)
        {
            this.Socket = Socket;
            this.ClientIP = (IPEndPoint)Socket.RemoteEndPoint;
        }

        public void SendDataToClient(byte[] Data, int offset, int length)
        {
            lock (Socket)
            {
                Socket.Send(Data, offset, length, SocketFlags.None);
            }
        }

        public void SendDataToClient(byte[] Data)
        {
            lock (Socket)
            {
                SendDataToClient(Data, 0, Data.Length);
            }
        }

        public void SendDataToServer(byte[] Data, int offset, int length)
        {
            lock (Socket)
            {
                ServerSocket.Send(Data, offset, length, SocketFlags.None);
            }
        }

        public void SendDataToServer(byte[] Data)
        {
            lock (Socket)
            {
                SendDataToServer(Data, 0, Data.Length);
            }
        }

        public void Disconnect()
        {
            if (_socket != null)
                _socket.Close();

            if (_serverSocket != null)
                _serverSocket.Close();

            ConnectionDisconnected();

            if (onConnectionDisconnected != null)
                onConnectionDisconnected(this);
        }

        internal void TriggerTargetServerEvents()
        {
            if (onSetTargetServer != null)
            {
                IPEndPoint TargetIp = onSetTargetServer(this);
                if (TargetIp != null)
                {
                    ServerIP = TargetIp;
                }
            }

            if (ServerIP == null)
                this.ServerIP = SetTargetServer();


            if (ServerIP != null)
            {
                //connect to the real Server
                _serverSocket = new Socket(AddressFamily.InterNetwork, SocketType.Stream, ProtocolType.Tcp);

                try
                {
                    _serverSocket.BeginConnect(ServerIP, ServerSocket_Connect, null);
                }
                catch { }
            }
        }

        private void ServerSocket_Connect(IAsyncResult ar)
        {
            try
            {
                _serverSocket.EndConnect(ar);
            }
            catch { }

            if (!_serverSocket.Connected)
            {
                Disconnect();
                return;
            }

            //start listening for traffic at Client and Server
            ClientBuffer = new byte[BUFFER_SIZE];
            ServerBuffer = new byte[BUFFER_SIZE];

            Socket.BeginReceive(ClientBuffer, 0, ClientBuffer.Length, SocketFlags.None, Socket_Receive, null);
            ServerSocket.BeginReceive(ServerBuffer, 0, ServerBuffer.Length, SocketFlags.None, Server_Receive, null);

            //let the user know we connected
            if (onConnectionEstablished != null)
                onConnectionEstablished(this);

            ConnectionEstablished();
        }

        private void Socket_Receive(IAsyncResult ar)
        {
            try
            {
                int ReceivedLen = Socket.EndReceive(ar);

                if (ReceivedLen > 0)
                {
                    byte[] Data = new byte[ReceivedLen];
                    Array.Copy(ClientBuffer, Data, ReceivedLen);

                    if (onReceiveDataFromClient != null)
                    {
                        onReceiveDataFromClient(this, Data);
                    }

                    ReceiveDataFromClient(Data);
                }
                else
                {
                    Disconnect();
                    return;
                }
            }
            catch
            {
                Disconnect();
                return;
            }

            ClientPacketsReceived++;

            try
            {
                Socket.BeginReceive(ClientBuffer, 0, ClientBuffer.Length, SocketFlags.None, Socket_Receive, null);
            }
            catch
            {
                //disconnected
                Disconnect();
            }
        }

        private void Server_Receive(IAsyncResult ar)
        {
            try
            {
                int ReceivedLen = ServerSocket.EndReceive(ar);

                if (ReceivedLen > 0)
                {
                    byte[] Data = new byte[ReceivedLen];
                    Array.Copy(ServerBuffer, Data, ReceivedLen);

                    if (onReceiveDataFromServer != null)
                    {
                        onReceiveDataFromServer(this, Data);
                    }

                    ReceiveDataFromServer(Data);
                }
                else
                {
                    Disconnect();
                    return;
                }
            }
            catch
            {
                Disconnect();
                return;
            }

            ServerPacketsReceived++;

            try
            {
                ServerSocket.BeginReceive(ServerBuffer, 0, ServerBuffer.Length, SocketFlags.None, Server_Receive, null);
            }
            catch
            {
                //disconnected
                Disconnect();
            }
        }
    }
}