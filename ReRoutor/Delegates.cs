﻿using ReRoutor.Clients;
using System;
using System.Collections.Generic;
using System.Net;
using System.Text;

namespace ReRoutor
{
    public delegate void ReceiveDataCallback(RouteSocket Client, byte[] Data);

    public delegate void IncomingConnectionCallback(RouteSocket Client);

    public delegate void ConnectionEstablishedCallback(RouteSocket Client);
    public delegate void ConnectionDisconnectedCallback(RouteSocket Client);

    public delegate IPEndPoint SetTargetServerCallback(RouteSocket Client);
}