﻿using ReRoutor.Clients;
using System;
using System.Collections.Generic;
using System.Net;
using System.Net.Sockets;
using System.Text;

namespace ReRoutor
{
    public class ReRouteServer
    {
        private Socket socket;
        private List<RouteSocket> RoutingSockets;
        public event IncomingConnectionCallback onIncomingConnection;
        public Type RouteSocketType { get; private set; }

        public ReRouteServer()
        {

        }

        /// <summary>
        /// Start the Server with a specific Route Socket in mind
        /// </summary>
        /// <param name="Port">The target port of listening at</param>
        /// <param name="RouteSocket">Type of the routing socket</param>
        public void StartServer(int Port, Type RouteSocketType)
        {
            if (onIncomingConnection == null)
                throw new NullReferenceException("onIncomingConnection is NULL");
            if (RouteSocketType.GetConstructors().Length == 0)
                throw new Exception("Not a valid RouteSocket Type");

            this.RouteSocketType = RouteSocketType;

            this.RoutingSockets = new List<RouteSocket>();
            this.socket = new Socket(AddressFamily.InterNetwork, SocketType.Stream, ProtocolType.Tcp);
            this.socket.Bind(new IPEndPoint(0, Port));
            this.socket.Listen(100);

            this.socket.BeginAccept(socket_Accept, null);
        }

        public void StopServer()
        {
            if(socket != null)
            {
                socket.Close();

                foreach (RouteSocket client in RoutingSockets)
                    client.Disconnect();
            }
        }

        private void socket_Accept(IAsyncResult ar)
        {

            Console.WriteLine("Incoming connection...");

            try
            {
                RouteSocket RoutingSocket = (RouteSocket)Activator.CreateInstance(RouteSocketType, socket.EndAccept(ar));

                if (onIncomingConnection != null)
                    onIncomingConnection(RoutingSocket);

                RoutingSocket.TriggerTargetServerEvents();

                RoutingSockets.Add(RoutingSocket);
            }
            catch { }

            this.socket.BeginAccept(socket_Accept, null);
        }
    }
}