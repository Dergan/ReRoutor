﻿using ReRoutor;
using ReRoutor.Clients.MineCraft;
using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.Net;
using System.Text;

namespace SampleProject
{
    class Program
    {
        static void Main(string[] args)
        {
            Console.Title = "ReRoutor Console";
            Console.WriteLine("Welcome to ReRoutor");
            Console.WriteLine("This sample project will show you some cool tricks we can perform");

            Console.WriteLine();
            Console.WriteLine("This sample will show you how easy it is to manipulate a game called MineCraft");
            Console.WriteLine("As example, go to \"Play Multiplayer\" and \"Add server\"");
            Console.WriteLine("Use the following ip address as test \"localhost\"");
            Console.WriteLine("Keep hitting Refresh to blow your mind");
            Console.WriteLine();

            Console.WriteLine("We're now listening for a MineCraft Game...");
            ReRouteServer routingServer = new ReRouteServer();

            routingServer.onIncomingConnection += routingServer_onIncomingConnection;
            routingServer.StartServer(25565, typeof(MineCraftAdvertisementClient));


            Process.GetCurrentProcess().WaitForExit();
        }

        static void routingServer_onIncomingConnection(ReRoutor.Clients.RouteSocket Client)
        {
            Client.onReceiveDataFromClient += Client_onReceiveDataFromClient;
            Client.onReceiveDataFromServer += Client_onReceiveDataFromServer;
            Client.onConnectionEstablished += Client_onConnectionEstablished;
        }

        static void Client_onConnectionEstablished(ReRoutor.Clients.RouteSocket Client)
        {
            Console.WriteLine("[" + Client.ClientIP.Address.ToString() + "] -> [" + Client.ServerIP.Address.ToString() + "]");
        }

        static void Client_onReceiveDataFromServer(ReRoutor.Clients.RouteSocket Client, byte[] Data)
        {
            
        }

        static void Client_onReceiveDataFromClient(ReRoutor.Clients.RouteSocket Client, byte[] Data)
        {
            
        }
    }
}